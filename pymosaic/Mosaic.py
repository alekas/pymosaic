'''
Created on Mar 16, 2014

@author: Andreas
'''

from PIL import Image
import math
import os.path
import numpy as np
import glob

#From http://stackoverflow.com/a/2270895/116915
def averageColor(filename):
    im = Image.open(filename)
    h = im.histogram()
     
    # split into red, green, blue
    r = h[0:256]
    g = h[256:256*2]
    b = h[256*2: 256*3]
    # perform the weighted average of each channel:
    # the *index* is the channel value, and the *value* is its weight
    return (
        sum( i*w for i, w in enumerate(r) ) / sum(r),
        sum( i*w for i, w in enumerate(g) ) / sum(g),
        sum( i*w for i, w in enumerate(b) ) / sum(b)
    )
    
def rgb_to_xyz(color):
    R = color[0] / 255.0
    G = color[1] / 255.0
    B = color[2] / 255.0
    
    gamma = 2.2
    
    r = math.pow(R, gamma)
    g = math.pow(G, gamma)
    b = math.pow(B, gamma)
    
    m = np.matrix([
        [0.4124564, 0.3575761, 0.1804375],
        [0.2126729, 0.7151522, 0.0721750],
        [0.0193339, 0.1191920, 0.9503041]])
    rgb = np.matrix([[r], [g], [b]])
    
    xyz = m*rgb
    return (xyz[0, 0]*100, xyz[1,0]*100, xyz[2,0]*100)

def xyz_to_lab(xyz):
    Xref=95.047#0.3127
    Yref=100.000#0.3290
    Zref=108.883#0.3583
    
    xref = xyz[0]/Xref
    yref = xyz[1]/Yref
    zref = xyz[2]/Zref
    
    
    fx = f(xref)
    fy = f(yref)
    fz = f(zref)
    
    L = 116.0*fy-16.0
    a = 500.0*(fx-fy)
    b = 200.0*(fy-fz)
    
    return (L, a, b)

def rgb_to_lab(rgb):
    return xyz_to_lab(rgb_to_xyz(rgb))

def f(ref, epsilon=0.008856, kappa=903.3):
    if ref>epsilon:
        fx = ref**(1/3.0) 
    else: 
        fx = (kappa * ref + 16.0)/116.0
        
    return fx

def deltaE(lab1, lab2):
    #print "Comparing lab1: "+str(lab1)+" and lab2: "+str(lab2)
    L1 = lab1[0]
    L2 = lab2[0]
    a1 = lab1[1]
    a2 = lab2[1]
    b1 = lab1[2]
    b2 = lab2[2]
    
    LprimeAvg = (L1+L2)/2
    C1 = np.sqrt(a1*a1+b1*b1)
    C2 = np.sqrt(a2*a2+b2*b2)
    
    Cavg = (C1+C2)/2
    
    G = (1-np.sqrt(Cavg**7/(Cavg**7+25**7)))/2
    
    a1prime = a1*(1+G)
    a2prime = a2*(1+G)
    
    C1prime = np.sqrt(a1prime*a1prime+b1*b1)
    C2prime = np.sqrt(a2prime*a2prime+b2*b2)
    
    CprimeAvg = (C1prime+C2prime)/2
    
    h1_cond = np.degrees(np.arctan2(b1,a1prime))
    if(h1_cond>=0):
        h1prime = h1_cond
    else:
        h1prime = h1_cond+360
        
    h2_cond = np.degrees(np.arctan2(b2,a2prime))
    if(h2_cond>=0):
        h2prime = h2_cond
    else:
        h2prime = h2_cond+360
    
    if np.abs(h1prime - h2prime) > 180:
        HprimeAvg = (h1prime+h2prime+360)/2
    else:
        HprimeAvg = (h1prime+h2prime)/2
        
    T = 1 - 0.17*np.cos(np.radians(HprimeAvg-30)) + 0.24*np.cos(np.radians(2*HprimeAvg))+0.32*np.cos(np.radians(3*HprimeAvg)+6) - 0.20*np.cos(np.radians(4*HprimeAvg - 63))
    if np.abs(h2prime-h1prime)<=180:
        deltahprime = h2prime - h1prime
    elif np.abs(h2prime - h1prime)>180 and h2prime <= h1prime:
        deltahprime = h2prime - h1prime + 360
    else:
        deltahprime = h2prime - h1prime - 360
        
    deltaLprime = L2-L1
    deltaCprime = C2prime - C1prime
    
    deltaHprime = 2*np.sqrt(C1prime*C2prime)*np.sin(np.radians(deltahprime/2))
    SL = 1 + (.015*(LprimeAvg-50)**2)/(np.sqrt(20+(LprimeAvg-50)**2))
    SC = 1 + .045*CprimeAvg
    SH = 1+.015*CprimeAvg*T
    deltaTheta = 30**(-(((HprimeAvg-275)/25)**2))
    RC = 2*np.sqrt((CprimeAvg**7)/(CprimeAvg**7 + 25**7))
    RT = -RC*np.sin(np.radians(2*deltaTheta))
    
    KL = KC = KH = 1
    
    deltaETerm1 = (deltaLprime/(KL*SL))**2
    deltaETerm2 = (deltaCprime/(KC*SC))**2
    deltaETerm3 = (deltaHprime/(KH*SH))**2
    deltaETerm4 = RT*(deltaCprime/(KC*SC))*(deltaHprime/(KH*SH))
    
    return np.sqrt(deltaETerm1 + deltaETerm2 + deltaETerm3 + deltaETerm4)
        
def createThumbnail(original_file_name, size):
    infile = "../picture_source/"+original_file_name
    outfile = "../picture_source/thumbnails/"+str(size[0])+"x"+str(size[1])+"_"+original_file_name
    if os.path.isfile(infile):
        if os.path.isfile(outfile):
            return
        else:
            i = Image.open(infile)
            i.thumbnail(size)
            i.save(outfile)
            
def cropToSquare(im):
    size = im.size
    w = size[0]
    h = size[1]
    new_rect = (0, 0, w, h)
    if(w<h): #width is smaller than height, so crop using width
        new_rect = (0, (h-w)/2, w, h-(h-w)/2)
    elif w>h:
        new_rect = ((w-h)/2, 0, w-(w-h)/2, h)
    return im.crop(new_rect)

def findClosestColor(color, average_color_list):
    difference = None
    least_different_file = ''
    for file in average_color_list:
        dE = deltaE(average_color_list[file], color)
        if difference == None or dE<difference:
            difference = dE
            least_different_file = file
    
    return least_different_file
            
if __name__ == '__main__':
    
    #white = rgb_to_lab((255, 255, 255))
    #magenta = rgb_to_lab((255, 0, 255))
    #diff = deltaE(white, magenta)
    #print diff
    factor = 8
    new_pixel_size = 32
    im = Image.open("../resources/emma.jpg")
    size1 = im.size
    print(size1)
    
    
    pixels = im.getdata()
    new_width = int(math.ceil(size1[0]/float(factor)))
    new_height = int(math.ceil(size1[1]/float(factor)))
    new_pixels = [(0,0,0)] * (new_width * new_height)
    lab_colors = [(0,0,0)] * (new_width * new_height)
    print("new_pixels: "+str(new_width)+" x "+str(new_height)+", "+str(len(new_pixels)))
    print("pixels: "+str(size1[0])+" x "+str(size1[1]))
    
    
    im2 = Image.new("RGB", (new_width, new_height))
    size2 = im2.size
    print(size2)
    
            
    #process library pictures, set to correct size, calculate average color, etc.
    library_directory = '../library/'
    originals_directory = library_directory+'originals\\'
    thumbnails_directory = library_directory+'thumbnails\\'
    thumbnail_size = 128, 128
    for infile in glob.glob(originals_directory+"*.jpg"):
        #print infile.replace(originals_directory, thumbnails_directory)
        thumbnail_filename = infile.replace(originals_directory, thumbnails_directory)
        if not os.path.isfile(thumbnail_filename):
            im = Image.open(infile)
            im = cropToSquare(im)
            im.thumbnail(thumbnail_size, Image.ANTIALIAS)
            im.save(thumbnail_filename)
    
    avg_colors = {}
    for infile in glob.glob(thumbnails_directory+"*.jpg"):
        avg_colors[infile] = rgb_to_lab(averageColor(infile))
        
    #convert to lab and set up smal thumbnail for reference
    mosaic = np.empty((size2[0], size2[1]), dtype="object").reshape(size2[0], size2[1])
    for y in xrange(0, size1[1], factor):
        for x in xrange(0, size1[0], factor):
            i = y/factor*size2[0]+x/factor
            new_pixels[i]=pixels[y*size1[0]+x]
            mosaic[x/factor, y/factor]=findClosestColor(rgb_to_lab(new_pixels[i]), avg_colors)
            
    im_mosaic = Image.new("RGB", (size2[0]*thumbnail_size[0], size2[1]*thumbnail_size[1]))
    size_mosaic = im_mosaic.size
    mosaic_pixels = [(0,0,0)] * (size_mosaic[0] * size_mosaic[1])
    print "Mosaic composite size: "+str(size_mosaic)
    print "Mosaic size: "+str(mosaic.shape);
    for y in xrange(0, mosaic.shape[1]):
        for x in xrange(0, mosaic.shape[0]):
            im_current = Image.open(mosaic[x, y])
            current_pixels = im_current.getdata()
            print "Filling "+str(y)+", "+str(x)+" with "+mosaic[x, y]
            #for cy in xrange(y*thumbnail_size[1], y*thumbnail_size[1]+thumbnail_size[1]):
            #    for cx in xrange(x*thumbnail_size[0], x*thumbnail_size[0]+thumbnail_size[0]):
            for cy in xrange(0, thumbnail_size[1]):
                for cx in xrange(0, thumbnail_size[0]):
                    dest_i = (y*thumbnail_size[1] + cy) * size_mosaic[0] + (x*thumbnail_size[0]+cx)
                    print "Position: "+str(cy)+", "+str(cx)+"->"+str(dest_i)
                    mosaic_pixels[dest_i] = current_pixels[cy*thumbnail_size[0]+cx]
    
    print mosaic
    print(len(new_pixels))
    im_mosaic.putdata(mosaic_pixels)
    im2.putdata(new_pixels)
    im2.save("../resources/Koopa2.png")
    im_mosaic.save("../resources/emma_mosaic.png")